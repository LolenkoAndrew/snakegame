#Snake Game

autor: Lolenko Andrey 
university: NTU "KhPI" 
group: KIT17b

---

###Something about game
This game is playble only in console mode
To control Snake use:
- W - up
- A - left
- S - down
- D - right

In development was used JUnit library (version 4.12) to create unit tests.

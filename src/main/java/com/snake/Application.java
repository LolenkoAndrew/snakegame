package com.snake;

import com.snake.builder.*;

/**
 * Class Application
 * contains psvm for start program
 * run this class for start "Snake Game"
 * w - UP
 * a - LEFT
 * s - DOWN
 * d - RIGHT
 */
public class Application {
    public static void main(String[] args) {
        TestGameBuilder game = new TestGameBuilder(20, 20);
        game.buildAndStartGame();
    }
}

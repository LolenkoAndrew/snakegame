package com.snake.model;

import java.util.Objects;

/**
 * Class Point
 * contain coordinates of point(x,y) and symbol of point
 */
public class Point {
    /**
     * x coordinate
     */
    private final int x;

    /**
     * y coordinate
     */
    private final int y;

    /**
     * symbol of point
     */
    private char symbol;

    /**
     * Override method equals
     * It was make for test and debug game
     * @param o Object
     * @return isEquals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y &&
                symbol == point.symbol;
    }

    /**
     * Override method hashCode
     * It was make for test and debug game
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(x, y, symbol);
    }

    /**
     * Override method toString
     * It was made for printing coordinates of point
     * @return toString
     */
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    /**
     * Constructor Point without symbol
     * @param x x coordinate
     * @param y y coordinate
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Constructor Point with symbol
     * @param x x coordinate
     * @param y y coordinate
     * @param symbol symbol
     */
    public Point(int x, int y, char symbol) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
    }

    /**
     * x coordinate getter
     * @return x
     */
    public int getX() {
        return x;
    }

    /**
     * y coordinate getter
     * @return y
     */
    public int getY() {
        return y;
    }

    /**
     * symbol getter
     * @return symbol
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * symbol setter
     * @param symbol symbol
     */
    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }
}

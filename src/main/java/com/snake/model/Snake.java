package com.snake.model;

import java.util.LinkedList;

/**
 * Class Snake
 */
public class Snake {
    /**
     * Constructor Snake with List of body points
     * @param body List of body points
     */
    public Snake(LinkedList<Point> body) {
        this.body = body;
    }

    /**
     * Empty Snake constructor
     */
    public Snake() {
    }

    /**
     * LinkedLint of body points
     */
    private LinkedList<Point> body = new LinkedList<>();

    /**
     * Method move
     * Move snake head and then snake tail to the head
     * @param direction direction
     * @return Point
     */
    public Point move(Direction direction) {
        Point point = null;
        int headX = this.body.getFirst().getX();
        int headY = this.body.getFirst().getY();
        switch (direction) {
            case UP:
                point = new Point(headX, headY - 1, '@');
                break;
            case RIGHT:
                point = new Point(headX + 1, headY, '@');
                break;
            case DOWN:
                point = new Point(headX, headY + 1, '@');
                break;
            case LEFT:
                point = new Point(headX - 1, headY, '@');
                break;
        }
        this.body.addFirst(point);
        return body.removeLast();
    }

    /**
     * isEatFood
     * return true if head point and food point equals
     * @param food food
     * @return isEatFood
     */
    public boolean isEatFood(Point food) {
        Point head = body.getFirst();
        return Math.abs(head.getX() - food.getX()) + Math.abs(head.getY() - food.getY()) == 0;
    }

    /**
     * Getter of snake head
     * @return head of snake
     */
    public Point getHead() {
        return body.getFirst();
    }

    /**
     * addTail
     * this method is add tail to point
     * @param area Point
     * @return area
     */
    public Point addTail(Point area) {
        this.body.addLast(area);
        return area;
    }

    /**
     * Snake body getter
     * @return body
     */
    public LinkedList<Point> getBody() {
        return body;
    }

    /**
     * Enum of directions for Snake
     * contains LEFT, RIGHT, UP and DOWN directions
     */
    public enum Direction {
        /**
         * Directions
         */
        LEFT, RIGHT, UP, DOWN;

        /**
         * compatibleWith
         * this method check if directions compatible with new direction
         * @param newDirection newDirection
         * @return isCompatibleWithNewDirection
         */
        public boolean compatibleWith(Direction newDirection) {
            if (this.equals(LEFT) || this.equals(RIGHT)) {
                return UP.equals(newDirection) || DOWN.equals(newDirection);
            } else {
                return LEFT.equals(newDirection) || RIGHT.equals(newDirection);
            }
        }
    }

    /**
     * Builder class for Snake
     * It can build and return snake by method build
     */
    public static class Builder {
        /**
         * List of body points
         */
        private LinkedList<Point> points;

        /**
         * Constructor for Builder by snake
         * @param snake snake
         * @return Builder
         */
        public Builder snake(Snake snake) {
            this.points = snake.body;
            return this;
        }

        /**
         * Constructor for Builder by List of points
         * @param points points
         * @return Builder
         */
        public Builder points(LinkedList<Point> points) {
            this.points = points;
            return this;
        }

        /**
         * method build
         * return new Snake
         * @return Snake
         */
        public Snake build() {
            return new Snake(points);
        }
    }
}
package com.snake.model;

import java.util.Scanner;

import static com.snake.model.Snake.Direction;

/**
 * Class Player
 * contains method for move snake
 */
public class Player {
    /**
     * doMove
     * method for move snake
     * @param board board
     * @param direction direction
     */
    public void doMove(Board board, Direction direction){
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        switch(s){
            case "w" :
                board.changeDirection(Direction.UP);
                break;
            case "a" :
                board.changeDirection(Direction.LEFT);
                break;
            case "s" :
                board.changeDirection(Direction.DOWN);
                break;
            case "d" :
                board.changeDirection(Direction.RIGHT);
                break;
        }
    }
}

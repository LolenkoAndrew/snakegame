package com.snake.model;

import java.util.Random;

import static com.snake.model.Snake.Direction;

/**
 * Class Board
 * contains Snake, Direction, Player and Food
 * Also contains methods for launching the game(like doGome)
 */
public class Board {
    /**
     * Boolean array isCovered[][]
     * contains true on points coordinates
     */
    private boolean isCovered[][];

    /**
     * Width on board
     */
    private final int width;

    /**
     * Height of board
     */
    private final int height;

    /**
     * Score
     * default is 0;
     */
    private int scores = 0;

    /**
     * Player
     * need to move snake
     */
    private Player player;

    /**
     * Snake
     * contains LinkedList of points
     */
    private Snake snake;

    /**
     * Food
     * generate randomly
     */
    private Point food;

    /**
     * Snake Direction
     * contain enum of direction
     * default is LEFT
     */
    private Direction snakeDirection = Direction.LEFT;

    /**
     * isDirectionChanged
     * contain true when direction is changed
     */
    public boolean isDirectionChanged = false;

    /**
     * Constructor of Board
     * initialized Snake, first food and Player
     * @param width width
     * @param height height
     */
    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        isCovered = new boolean[width][height];
        this.player = new Player();
        initSnake();
        createFood();
    }

    /**
     * goGame
     * Main method of game
     * It start game and print board in console
     */
    public void doGame() {
        while (true) {
            printBoard();
            isDirectionChanged = false;
            player.doMove(this, snakeDirection);
            if (nextRound() == true) {
                printBoard();
            } else {
                System.out.print("Your scores: " + getScore());
                break;
            }
        }
    }

    /**
     * initSnake
     * this method is initialized Snake and put it on Board(center)
     * @return snake
     */
    private Snake initSnake() {
        snake = new Snake();
        for (int i = 0; i < 3; i++) {
            snake.addTail(new Point(i + width / 2, height / 2, '@'));
            isCovered[i + width / 2][height / 2] = true;
        }
        return snake;
    }

    /**
     * createFood
     * this method is create Food and put it randomly on Board
     * @return food
     */
    public Point createFood() {
        int x, y;
        do {
            x = new Random().nextInt(width);
            y = new Random().nextInt(height);
        } while (isCovered[x][y] == true);
        food = new Point(x, y, '*');
        return food;
    }

    /**
     * nextRound
     * return true and make next round if it is valid
     * @return isNextRoundValid
     */
    public boolean nextRound() {
        if (isMoveValid(snakeDirection)) {
            Point move = snake.move(snakeDirection);
            if (snake.isEatFood(food)) {
                snake.addTail(move);
                createFood();
                System.out.println(++scores);
            } else isCovered[move.getX()][move.getY()] = false;
            return true;
        } else return false;
    }

    /**
     * isMoveValid
     * return true if next move by the direction is valid
     * @param direction direction
     * @return isMoveValid
     */
    private boolean isMoveValid(Direction direction) {
        int headX = snake.getHead().getX();
        int headY = snake.getHead().getY();
        switch (direction) {
            case UP:
                headY--;
                break;
            case RIGHT:
                headX++;
                break;
            case DOWN:
                headY++;
                break;
            case LEFT:
                headX--;
                break;
        }
        if (headX < 0 || headX >= width || headY < 0 || headY >= height) return false;
        if (isCovered[headX][headY] == true) return false;
        isCovered[headX][headY] = true;
        return true;
    }

    /**
     * changeDirection
     * this method is change Direction for newDirection
     * @param newDirection newDirection
     */
    public void changeDirection(Direction newDirection) {
        if (snakeDirection.compatibleWith(newDirection)) {
            snakeDirection = newDirection;
            isDirectionChanged = true;
        }
    }

    /**
     * printBoard
     * this method is print Board with snake and food to console
     * snake         - '@'
     * food          - '*'
     * empty space   - '.'
     */
    public void printBoard() {
        for (Point point : snake.getBody()) {
            System.out.println(point.toString());
        }
        System.out.println("--------------------------------");
        char[][] boardChars = new char[height][width];
        for (Point point : snake.getBody()) {
            boardChars[point.getX()][point.getY()] = '@';
        }
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                if (boardChars[j][i] != '@' && food.getY() == i && food.getX() == j) {
                    boardChars[j][i] = '*';
                } else if (boardChars[j][i] != '@') {
                    boardChars[j][i] = '.';
                }
                System.out.print(boardChars[j][i]);
            }
            System.out.println();
        }
        System.out.println("--------------------------------");
        System.out.println();

    }

    /**
     * Snake getter
     * @return snake
     */
    public Snake getSnake() {
        return snake;
    }

    /**
     * Food getter
     * @return food
     */
    public Point getFood() {
        return food;
    }

    /**
     * Width getter
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Height getter
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Score getter
     * @return scores
     */
    public int getScore() {
        return scores;
    }

    /**
     * Player getter
     * @return player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Direction getter
     * @return snakeDirection
     */
    public Direction getSnakeDirection() {
        return snakeDirection;
    }

}

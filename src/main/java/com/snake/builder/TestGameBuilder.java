package com.snake.builder;

import com.snake.model.Board;

import java.util.logging.Logger;

public class TestGameBuilder {
    /**
     * Logger for test game
     */
    private static final Logger LOGGER = Logger.getLogger("com.snake.builder.TestGameBuilder");
    /**
     * DEFAULT_GRID_WIDTH
     */
    private final int DEFAULT_GRID_WIDTH;

    /**
     * DEFAULT_GRID_HEIGHT
     */
    private final int DEFAULT_GRID_HEIGHT;

    /**
     * Constructor for GameBuilder
     * @param DEFAULT_GRID_WIDTH DEFAULT_GRID_WIDTH
     * @param DEFAULT_GRID_HEIGHT DEFAULT_GRID_HEIGHT
     */
    public TestGameBuilder(int DEFAULT_GRID_WIDTH, int DEFAULT_GRID_HEIGHT) {
        LOGGER.info("Start init game");
        this.DEFAULT_GRID_WIDTH = DEFAULT_GRID_WIDTH;
        this.DEFAULT_GRID_HEIGHT = DEFAULT_GRID_HEIGHT;
        LOGGER.info("End init game");
    }

    /**
     * buildAndStartGame
     * can build and run game
     */
    public void buildAndStartGame(){
        Board board = new Board(DEFAULT_GRID_WIDTH, DEFAULT_GRID_HEIGHT);
        LOGGER.info("Running game...");
        board.doGame();
    }
}

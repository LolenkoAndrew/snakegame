package com.snake.builder;

import com.snake.model.Board;

/**
 * Class GameBulider
 * contains width and height of game
 * can build and run game
 */
public class GameBuilder {
    /**
     * DEFAULT_GRID_WIDTH
     */
    private final int DEFAULT_GRID_WIDTH;

    /**
     * DEFAULT_GRID_HEIGHT
     */
    private final int DEFAULT_GRID_HEIGHT;

    /**
     * Constructor for GameBuilder
     * @param DEFAULT_GRID_WIDTH DEFAULT_GRID_WIDTH
     * @param DEFAULT_GRID_HEIGHT DEFAULT_GRID_HEIGHT
     */
    public GameBuilder(int DEFAULT_GRID_WIDTH, int DEFAULT_GRID_HEIGHT) {
        this.DEFAULT_GRID_WIDTH = DEFAULT_GRID_WIDTH;
        this.DEFAULT_GRID_HEIGHT = DEFAULT_GRID_HEIGHT;
    }

    /**
     * buildAndStartGame
     * can build and run game
     */
    public void buildAndStartGame(){
        Board board = new Board(DEFAULT_GRID_WIDTH, DEFAULT_GRID_HEIGHT);
        board.doGame();
    }
}
